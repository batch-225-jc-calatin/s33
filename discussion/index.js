// [SECTION] Javascript Synchronous vs Asychronous
// Javacript is by default is synchronous meaning that only one statement is executed at a time

console.log("Hello");

console.log("Goodbye");



// Asynchronous means that we can proceed to execute other staments, while time consuming code is running in the background.

//The Fetch API

/*
	- Allows you to asynchronous request for a resource (data)
	- A "promise" is an object that represents the eventual completion (or failure) of an asynchronous functioon and it's resulting value
	
	 it returns
	-result or data
	- pending
	- error

	Syntax :
	fetch('URL')

*/
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

/*

*/